@testset "power_regression.jl" begin
    res = 0

    relu(x) = (x > 0) * x

    i1, i2 = [x for x = 1:10, y = 1:10][:], [y for x = 1:10, y = 1:10][:]
    powers = @. relu(i1 - 3) + relu(i2 - 3)
    currents = [i1 i2]'
    @test (res = VCSELArrayAnalysisML.model_power(currents, powers, Nepoch = 10); true)
    @test (
        res = VCSELArrayAnalysisML.model_power(
            currents,
            powers,
            Nepoch = 10,
            filtercoherent = true,
        );
        true
    )
    @test (
        res = VCSELArrayAnalysisML.model_power(
            currents,
            powers,
            Nepoch = 10,
            filterthreshold = true,
        );
        true
    )
    @test (
        res = VCSELArrayAnalysisML.model_power(
            currents,
            powers,
            Nepoch = 10,
            negativepowerloss = true,
            overestimationloss = true,
        );
        true
    )
end
