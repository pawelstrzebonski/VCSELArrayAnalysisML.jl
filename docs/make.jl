using Documenter
import VCSELArrayAnalysisML

makedocs(
    sitename = "VCSELArrayAnalysisML.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "VCSELArrayAnalysisML.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["power_regression.jl" => "power_regression.md"],
        "test/" => ["power_regression.jl" => "power_regression_test.md"],
    ],
)
