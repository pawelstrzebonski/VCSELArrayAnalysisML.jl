# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Optimizations
* Improvements to ease and effectiveness of power modeling (less parameter tuning, better convergence to uncoupled power estimate)

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to minimize the amount of code and function repetition
