# VCSELArrayAnalysisML.jl Documentation

## About

`VCSELArrayAnalysisML.jl` is a Julia package for analyzing measurements
of coherent VCSEL arrays and characterizing their coherence using
machine learning methods.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl
```

## Features

* Artificial neural network for current-power regression (used for estimating coherent power enhancement)

## Related Packages

* [VCSELArrayAnalysis.jl](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl) provides other functionality relevant to characterizing coherent laser arrays using non-machine-learning methods
