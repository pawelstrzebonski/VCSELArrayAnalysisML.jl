# power_regression.jl

## Description

This file implements artificial neural networks for modeling the
current-power relation given a set of measured data. It is presumed
that the network will fit the majority of the dataset, presumably
uncoupled regime, better than the points in the coherently coupled regime,
thus enabling an estimation of the uncoupled array power throughout
the operating space.

## Functions

```@autodocs
Modules = [VCSELArrayAnalysisML]
Pages   = ["power_regression.jl"]
```
