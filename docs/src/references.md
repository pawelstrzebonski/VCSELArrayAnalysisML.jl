# References

This package implements some of the methods described in:

[P. Strzebonski, H. Dave, K. Lakomy, N. Jahan, W. North, K. Choquette, "Computational methods for VCSEL array characterization and control," Proc. SPIE 11704, Vertical-Cavity Surface-Emitting Lasers XXV, 117040L (5 March 2021), doi: 10.1117/12.2585066](https://doi.org/10.1117/12.2585066)

It can be cited using the following bibtex:

```
@inproceedings{Strzebonski2021,
  doi = {10.1117/12.2585066},
  url = {https://doi.org/10.1117/12.2585066},
  year = {2021},
  month = mar,
  publisher = {{SPIE}},
  author = {Pawel Strzebonski and Harshil Dave and Katherine Lakomy and Nusrat Jahan and William North and Kent Choquette},
  editor = {Kent D. Choquette and Chun Lei},
  title = {Computational methods for {VCSEL} array characterization and control},
  booktitle = {Vertical-Cavity Surface-Emitting Lasers {XXV}}
}
```

That manuscript should explain or cite the basic theoretical aspects of
coherent coupling in VCSEL arrays as relevant to experimental measurement
and data analysis, as well as most of the algorithms implemented here.
If you find this package or the methods it implements useful in your
work, please cite the above manuscript.
