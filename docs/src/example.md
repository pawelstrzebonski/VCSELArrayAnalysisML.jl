# Example

This package is used via the `model_power` function that takes the
current and power values as inputs and outputs a machine learning model
that can be used to predict the power as a function of driving currents.
Usage of this function is as follows:

```Julia
# Import the package
import VCSELArrayAnalysisML

# Load the measurement data (specific to your process flow)
current1, current2, ..., power=loadmeasurements()

# Convert the measurements to the input format
currents=[current1 current2 ...]

# Generate and train a model for the power
model=VCSELArrayAnalysisML.model_power(currents, power)

# Predict the power given a set of currents
power_predicted=model(currents)
```

The `model_power` function should work given any number of array elements
and currents, however to get good modeling performance you may need to
tune the model, dataset, and training parameters. For more information
on the available parameters, check the documentation for `model_power`
(in the Julia REPL, type in `?VCSELArrayAnalysisML.model_power` to get
the documentation).

For potentially faster model training, you may want to use GPU acceleration.
Please consult
[Flux](https://fluxml.ai/)
documentation (package used for machine learning) for specifics of GPU
acceleration support. If the correct GPU packages are installed and loaded
(for example, `CuArrays` for NVIDIA CUDA acceleration), it should be enough
to set `useGPU=true` in `model_power` for the function to use your
GPU for training.
