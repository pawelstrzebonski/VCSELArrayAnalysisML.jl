# VCSELArrayAnalysisML

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/VCSELArrayAnalysisML.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl/commits/master)

## About

`VCSELArrayAnalysisML.jl` is a Julia package for analyzing measurements
of coherent VCSEL arrays and characterizing their coherence using
machine learning methods.

## Features

* Artificial neural network for current-power regression (used for estimating coherent power enhancement)

Other relevant functionality that does not depend on artificial neural
networks is provided by the sister package
[VCSELArrayAnalysis.jl](https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysis.jl).

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/VCSELArrayAnalysisML.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for VCSELArrayAnalysisML.jl](https://pawelstrzebonski.gitlab.io/VCSELArrayAnalysisML.jl/).
