import Flux

"""
    customtrain(
		model,
		dataset,
		loss::Function;
		Nepoch::Integer = 100,
		loss_threshold::Number = 1e-5,
		opt = Flux.ADAM(),
	)->trained_model

A customized model training routine.

# Arguments:
- `model`: a Flux neural network model.
- `dataset`: dataset to train the network on.
- `loss::Function`: loss function to evaluate performance of network.
- `Nepoch::Integer`: (maximal) number of epochs to run training.
- `loss_threshold::Number`: loss threshold below which we finish training early.
- `opt`: a Flux optimization function.
"""
function customtrain(
    model,
    dataset,
    loss::Function;
    Nepoch::Integer = 100,
    loss_threshold::Number = 1e-5,
    opt = Flux.ADAM(),
)
    @info string("Beginning training loop...")
    best_loss = 1e9
    last_improvement = 0
    ps = Flux.params(model)
    best_model = deepcopy(model)
    for epoch_idx = 1:Nepoch
        # Train for a single epoch
        Flux.train!(loss, ps, dataset, opt)

        # Calculate accuracy:
        totalloss = Flux.mean([loss(x...) for x in dataset])

        @info string("Epoch #", epoch_idx, ": Loss=", totalloss)
        # If our accuracy is good enough, quit out.
        if totalloss <= loss_threshold
            @info string(
                " -> Early-exiting: We reached our target loss of ",
                loss_threshold,
            )
            break
        end

        # If this is the best accuracy we've seen so far, save the model out
        if totalloss < best_loss
            @info string(" -> New best accuracy!")
            best_model = deepcopy(model)
            best_loss = totalloss
            last_improvement = epoch_idx
        end

        # If there hasn't been an improvement in a while, call it quits
        if epoch_idx - last_improvement >= 30
            @warn string(" -> We're calling this converged.")
            break
        end
    end
    # Return the best performing model
    best_model
end

"""
    li2data(currents::AbstractMatrix, powers::AbstractVector; batchsize::Number = 100)

Formats the `currents` and `powers` into a dataset apropriate for Flux
training (split into batches of `batchsize` points, and shuffles the points).
"""
li2data(currents::AbstractMatrix, powers::AbstractVector; batchsize::Number = 100) =
    Flux.Data.DataLoader(
        (currents, powers[:, :]'),
        batchsize = batchsize,
        shuffle = true,
        partial = false,
    )

"""
    model_power(
		currents::AbstractMatrix,
		powers::AbstractVector;
		filtercoherent::Bool = false,
		filterthreshold::Bool = false,
		Nepoch::Integer = 1000,
		threshold::Number = 3,
		N::Integer = 3,
		layers::Integer = 2,
		batchsize::Integer = 100,
		overestimationloss::Bool = false,
		negativepowerloss::Bool = false,
		loss_threshold::Number = 1e-2,
		activationfunction::Function = Flux.leakyrelu,
		opt = Flux.ADAM(),
		useGPU::Bool = false,
	)->model

Trains a `Flux` artificial neural network to model the current to power
relation.

# Arguments:
- `currents::AbstractMatrix`: current value matrix of size `(Ncurrents, Npoints)`.
- `powers::AbstractVector`: vector of power values.
- `filtercoherent::Bool`: whether to use a trained model to predict coherence and exclude potentially coherent points from a subsequent training round.
- `filterthreshold::Bool`: whether to exclude points where all lasers are above threshold.
- `Nepoch::Integer`: (maximal) number of network training epochs.
- `threshold::Number`: threshold current for (optional) data filtering.
- `N::Integer`: width of hidden network layers.
- `layers::Integer`: number of hidden network layers
- `batchsize::Integer`: number of points per training batch.
- `overestimationloss::Bool`: whether to punish overestimating the power (giving negative power enhancement).
- `negativepowerloss::Bool`: whether to punish estimating power to be negative.
- `loss_threshold::Number`: loss threshold below which we finish training early.
- `activationfunction::Function`: activation function for the network.
- `opt`: optimization function for Flux's network training.
- `useGPU::Bool`: whether to use a GPU for network training.
"""
function model_power(
    currents::AbstractMatrix,
    powers::AbstractVector;
    filtercoherent::Bool = false,
    filterthreshold::Bool = false,
    Nepoch::Integer = 1000,
    threshold::Number = 3,
    N::Integer = 3,
    layers::Integer = 2,
    batchsize::Integer = 100,
    overestimationloss::Bool = false,
    negativepowerloss::Bool = false,
    loss_threshold::Number = 1e-2,
    activationfunction::Function = Flux.leakyrelu,
    opt = Flux.ADAM(),
    useGPU::Bool = false,
)
    @assert size(currents, 2) == length(powers)
    Ncurrents = size(currents, 1)

    # Convert data to Flux inputs, remove above multiple threshold data if desired
    ftmask = vec(count(>(threshold), currents, dims = 1) .<= 1)
    dataset = filterthreshold ?
        li2data(currents[:, ftmask], powers[ftmask], batchsize = batchsize) :
        li2data(currents, powers, batchsize = batchsize)

    # Create Flux model
    model = Flux.Chain(
        Flux.Dense(Ncurrents, N, activationfunction),
        fill(Flux.Dense(N, N, activationfunction), layers - 1)...,
        Flux.Dense(N, 1),
    )

    # If desired, shift stuff to GPU for training
    if useGPU
        model = Flux.gpu(model)
        dataset = Flux.gpu(dataset)
    end

    # Setup and train the model (reconstruction loss, overestimation loss, negative power loss)
    # Huber loss may be more resilient to outliers (coherence) as it is mse close to zero, mae away from zero
    loss(x, y) =
    #Flux.mae(model(x), y) +
        Flux.huber_loss(model(x), y, δ = 0.1) +
        (overestimationloss ? Flux.mean(Flux.relu.(model(x) .- y)) : 0) +
        (negativepowerloss ? Flux.mean(Flux.relu.(-model(x))) : 0)

    # Train the network
    model = customtrain(
        model,
        dataset,
        loss,
        Nepoch = Nepoch,
        opt = opt,
        loss_threshold = loss_threshold,
    )

    #TODO: Test and improve this section (expose power enhancement threshold?)
    # If desired...
    if filtercoherent
        # Return model to CPU...
        model = Flux.cpu(model)
        # Use the trained model to predict the power...
        powers_fitted = model(currents)[:]
        # Predict the power enhancement...
        power_enhancement = @. powers - powers_fitted
        # And filter out points that have "significant" power enhancement...
        mask = power_enhancement .< maximum(power_enhancement) / 2
        mask = filterthreshold ? mask .| ftmask : mask
        dataset = li2data(currents[:, mask], powers[mask], batchsize = batchsize)

        # If desired, shift stuff to GPU for training
        if useGPU
            model = Flux.gpu(model)
            dataset = Flux.gpu(dataset)
        end

        # And re-train the model (hopefully) excluding the coherent points from the training set
        model = customtrain(
            model,
            dataset,
            loss,
            Nepoch = Nepoch,
            opt = opt,
            loss_threshold = loss_threshold,
        )
    end

    # Return model to CPU
    model = Flux.cpu(model)

    # Return the trained model
    model
end
